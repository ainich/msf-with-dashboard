# Metasploit Installation with grafana dashboard

![screenshot](https://grafana.com/api/dashboards/9732/images/6104/image)

    sudo apt-get install postgresql

    sudo apt-get install curl

    curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > msfinstall && \
      chmod 755 msfinstall && \
      ./msfinstall

    wget -q -O - https://packages.grafana.com/gpg.key | sudo apt-key add -
    echo "deb https://packages.grafana.com/enterprise/deb stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list

    sudo apt-get install apt-transport-https

    sudo apt-get update

    sudo apt-get install grafana-enterprise

    sudo /bin/systemctl daemon-reload

    sudo /bin/systemctl enable grafana-server

    sudo grafana-cli plugins install grafana-piechart-panel

    sudo service grafana-server start

    msfdb init
    
    sudo apt-get install nmap
    
    cd

    cat .msf4/database.yml

add datasource postges (127.0.0.1:5433) for grafana via web interface http://grafanahost:3000

download dashbord from https://grafana.com/api/dashboards/9732/revisions/1/download

import downloaded dashboard for grafana via web interface
